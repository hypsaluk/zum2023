# represents 2D [x, y] cords
class Cords:
    x = -1
    y = -1

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def print(self):
        print("[" + str(self.x) + ", " + str(self.y) + "]", end="")