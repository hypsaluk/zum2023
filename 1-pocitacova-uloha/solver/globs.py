# global constants, variables and function used in all the Solver program

wallChar = "X"
freshChar = " "
startChar = "S"
endChar = "E"
openedChar = "?"
closedChar = "#"
pathChar = "o"
processedChar = "P"

def pri(line):
    print(line, end="")