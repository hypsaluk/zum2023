from solver import Solver
import sys

# parametry
    # string cestaKDatasetu
    # int výběr alg
    # bool krokuj

# postup
    # načti parametry z command line
    # naparsuj dataset z txt do 2D pole objektů vertex
    # zavolej příslušný alg
    # otevírání vrcholů a hledání cesty - krokuj/nekrokuj
    # alg vrátí upravené 2D pole, cestu a parenty
    # zobrazení finálního stavu

# provides list of console params; error -> False
def getConsoleParams():
    # sys.argv contains params
    print(sys.argv[1])
    print(sys.argv[2])
    print(sys.argv[3])
    try:
        dataPath = str(sys.argv[1])
        algId = int(sys.argv[2])
        pacing = bool(str(sys.argv[3]).lower() == "true")
    except:
        return False
    return [dataPath, algId, pacing]

def main():
    print("start of main() in Solver")
    params = getConsoleParams()
    if (params == False):
        print("Error: not valide parameters")
        print("usage: python solver/main.py [dataset-path] [alg-id] [showSteps]")
        return
    else:
        path = params[0]
        algId = params[1]
        pacing = params[2]

    # valid params -> convert data set to 2D array
    maze = Solver.prepareMaze(path)
        # maze.printInfo() prints info about vertexes line by line
    maze.printMap()
    # we have prepared maze
    # now choose alg and put the maze through it
    solvedMaze = Solver.solveMaze(algId, maze, pacing)
    # show solved maze
    solvedMaze.printMap()
    solvedMaze.printLegend()
    print()

if __name__ == '__main__':
    main()
