from vertex import Vertex
from cords import Cords
from vertex import State
from globs import *

# the Maze class represents 2D maze as 2D structure of vertexes and edges
# provides info about set of vertexes, their neighbours and metrics/heuristics

class Maze:
    vers = []  # Array of Vertex Arrays; vertexes of state space - indexation in the 2D array is like [y, x] !!!
    xLen = 0 # int; number of vertexes in row
    yLen = 0 # int; number of vertexes in collumn
    sVertex = None  # Vertex; starting vertex for path finding
    tVertex = None  # Vertex; target vertex for path finding
    vertexesExpanded = 0  # int; number of expanded vertexes during run of path finding alg
    pathLen = 0  # int; number of vertexes in found path

    # INITING


    # textMaze = Array of Strings corresponding to 1:1 maze
    def __init__(self, textMaze, sCords, tCords):
        self.initMaze(textMaze)
        self.sVertex = self.vers[sCords.y][sCords.x]
        self.tVertex = self.vers[tCords.y][tCords.x]

    # parses 2D Array of Strings representing maze to inner structures;
    # returns True
    # error -> False
    def initMaze(self, lines):
        self.xLen = len(lines[0])
        self.yLen = len(lines)
        print("\n> Maze.initMaze(); \n ininting Maze object; number of rows(x) and lines(y): " + str(self.xLen) + " " + str(self.yLen))
        self.vers = []
        for j in range(self.yLen): # iterates over lines (j <-> y)
            line = lines[j]
            versLine = []
            for i in range(self.xLen):  # iterating over one line (i <-> x) -> same y, x changes
                c = line[i]
                vAccess = (c != 'X')
                v = Vertex(Cords(i, j), vAccess)
                versLine.append(v)
            self.vers.append(versLine)

        return True


    # PRINTING


    def printInfo(self):
        x = self.xLen
        y = self.yLen
        print("x and y: " + str(x) + " " + str(y))
        print("s vertex: ", end="")
        self.sVertex.print()
        print()
        print("t vertex: ", end="")
        self.tVertex.print()
        print()
        for j in range(y):
            for i in range(x):
                v = self.vers[j][i]
                print(str(j) + " " + str(i) + " - ", end="")
                v.print()
                pri("; " + self.getVertexChar(v))
                vNbs = self.getNbs(v)
                print("; nbrs: ", end="")
                for nb in vNbs:
                    nb.cords.print()
                print()

    # prints the output of path-search alg
    def printMap(self):
        x = self.xLen
        y = self.yLen

        # print top
        pri("  ")
        for i in range(x):
            pri((i+1) % 10)
        print()

        # print 2D maze, scale 1:1
        for j in range(y):
            pri((j) % 10)
            for i in range(x):
                v = self.vers[j][i]
                if v.state == State.Processed:
                    vChar = processedChar
                elif (self.sVertex.cords.x == i) and (self.sVertex.cords.y == j):
                    vChar = startChar
                elif (self.tVertex.cords.x == i) and (self.tVertex.cords.y == j):
                    vChar = endChar
                else:
                    vChar = self.getVertexChar(v)
                print(vChar, end="")
            print()

    def printLegend(self):
        print("---------------------------------------")
        print("S Start")
        print("E End")
        print("# Opened node")
        print("o Path")
        print("X wall")
        print("---------------------------------------")
        print("Nodes expanded: " + str(self.vertexesExpanded))
        print("Path length: " + str(self.pathLen))


    # INFO ABOUT NBRS/HEURISTICS ETC.

    # returns list of [x, y] cords of all neighbours of given vertex (possibly 0 to 4 nbs per vertex)
    def getNbs(self, v):
        if not v.isAccess: return []  # not accessible vertex -> not connected with any nbr
        vx = v.cords.x
        vy = v.cords.y
        nbs = []
        for relCords in [[-1, 0], [1, 0], [0, -1], [0, 1]]:
            nx = vx + relCords[0]
            ny = vy + relCords[1]
            if self.validX(nx) and self.validY(ny):  # check, if not out of 2D maze
                # print("valid x and y")
                if self.vers[ny][nx].isAccess:
                    # print("appending")
                    nbs.append(self.vers[ny][nx])
        return nbs

    def countManhattan(self):
        tCords = self.tVertex.cords
        for j in range(0, self.yLen):
            for i in range(0, self.xLen):
                v = self.vers[j][i]
                vCords = v.cords
                heur = abs(tCords.x - vCords.x) + abs(tCords.y - vCords.y)
                self.vers[j][i].h = heur

    # GETTERS/SETTERS AND HELP METHODS

    def getVertexChar(self, v):
        if not v.isAccess:
            return wallChar
        elif v.inPath:
            return pathChar
        else:
            match v.state:
                case State.Open:
                    return openedChar
                case State.Closed:
                    return closedChar
                case State.Processed:
                    return processedChar
                case _:
                    return freshChar  # default
    def getFreshNbs(self, v):
        allNbs = self.getNbs(v)
        freshNbs = []
        for nb in allNbs:
            if nb.state == State.Fresh:
                freshNbs.append(nb)
        # TODO shuffle
        return freshNbs

    def validX(self, x):
        return (x >= 0) and (x < self.xLen)

    def validY(self, y):
        return (y >= 0) and (y < self.yLen)
