from vertex import State
from maze import Maze
import random
import time
import queue
from collections import deque

# finds path in given maze from his s vertex to t vertex

class PathFinder:

    pacing = False

    def __init__(self, pacing):
        random.seed((round(time.time() * 1000)))
        self.pacing = pacing

    def findPathInMaze(self, algId, maze):
        if maze.sVertex == maze.tVertex:
            print("The start vertex and target vertex are the same!")
            return maze
        print("\nSearching for path with alg num " + str(algId) + "\n")
        # change atr of vers in maze
        match algId:
            case 1:
                maze = self.__algRandom(maze)
            case 2:
                maze = self.__algBfs(maze)
            case 3:
                maze = self.__algDfs(maze)
            case 4:
                maze = self.__algGreedy(maze)
            case 5:
                maze = self.__algAStar(maze)
            case _:
                maze = self.__algRandom(maze) # default
        # on end, reconstruct path, count pathLen and set vertexes inPath atribute
        maze = self.reconstructPath(maze)
        maze = self.countNodesExpanded(maze)
        return maze

    def reconstructPath(self, maze):
        if maze.sVertex == maze.tVertex:
            maze.pathLen = 0
            return maze
        maze.pathLen = 1
        p = maze.tVertex.parent
        while not ((p == maze.sVertex) or (p == None)):
            maze.pathLen = (maze.pathLen + 1)
            p.inPath = True
            p = p.parent
        return maze

    def countNodesExpanded(self, maze):
        num = 0
        for j in range(0, maze.yLen):
            for i in range(0, maze.xLen):
                if maze.vers[j][i].state != State.Fresh:
                    num = num + 1
        maze.vertexesExpanded = num
        return maze
    def __algAStar(self, maze):
        maze.countManhattan()
        opened = [maze.sVertex]
        maze.sVertex.g = 0
        foundTarget = False
        while opened and (not foundTarget):  # continue till there are some opened vertexes
            # choose vertex with lowest heuristic value
            minF = opened[0].f()
            for n in opened:
                minF = min(minF, n.f())
            for n in opened:
                if n.f() == minF:
                    v = n
                    opened.remove(n)
                    break
            # process v with lowest f()
            if self.pacing:
                print("processing: " + str(v.cords.x) + " " + str(v.cords.y))
            v.state = State.Processed
            freshNbrs = maze.getFreshNbs(v)
            for nb in freshNbrs:
                nb.state = State.Open
                nb.parent = v
                nb.g = (v.g + 1)
                if nb == maze.tVertex:
                    foundTarget = True
                    if self.pacing:
                        print("!! Found target !!")
                    break  # found the target vertex
                opened.append(nb)
                if self.pacing:
                    print("opened nbr: " + str(nb.cords.x) + " " + str(nb.cords.y))
            if self.pacing:
                print("closed: " + str(v.cords.x) + " " + str(v.cords.y))
                maze.printMap()
                input()
                print()
            v.state = State.Closed  # closed opened vertex
        return maze

    def __algGreedy(self, maze):
        maze.countManhattan()
        opened = [maze.sVertex]
        foundTarget = False
        while opened and (not foundTarget):  # continue till there are some opened vertexes
            # choose vertex with lowest heuristic value
            minH = opened[0].h
            for n in opened:
                minH = min(minH, n.h)
            for n in opened:
                if n.h == minH:
                    v = n
                    opened.remove(n)
                    break
            # process v with lowest h
            if self.pacing:
                print("processing: " + str(v.cords.x) + " " + str(v.cords.y))
            v.state = State.Processed
            freshNbrs = maze.getFreshNbs(v)
            for nb in freshNbrs:
                nb.state = State.Open
                nb.parent = v
                if nb == maze.tVertex:
                    foundTarget = True
                    if self.pacing:
                        print("!! Found target !!")
                    break  # found the target vertex
                opened.append(nb)
                if self.pacing:
                    print("opened nbr: " + str(nb.cords.x) + " " + str(nb.cords.y))
            if self.pacing:
                print("closed: " + str(v.cords.x) + " " + str(v.cords.y))
                maze.printMap()
                input()
                print()
            v.state = State.Closed  # closed opened vertex
        return maze

    def __algDfs(self, maze):
        opened = deque()
        opened.append(maze.sVertex)
        foundTarget = False
        while opened and (not foundTarget):  # continue till there are some opened vertexes
            v = opened.pop()
            if self.pacing:
                print("processing: " + str(v.cords.x) + " " + str(v.cords.y))
            v.state = State.Processed
            freshNbrs = maze.getFreshNbs(v)
            if freshNbrs:
                nb = freshNbrs[0]
                nb.state = State.Open
                nb.parent = v
                if nb == maze.tVertex:
                    foundTarget = True
                    if self.pacing:
                        print("!! Found target !!")
                    break  # found the target vertex
                opened.append(v)
                opened.append(nb)
                if self.pacing:
                    print("opened nbr: " + str(nb.cords.x) + " " + str(nb.cords.y))
            if self.pacing:
                print("closed: " + str(v.cords.x) + " " + str(v.cords.y))
                maze.printMap()
                input()
                print()
            v.state = State.Closed  # closed opened vertex
        return maze

    def __algBfs(self, maze):
        opened = queue.Queue()
        opened.put(maze.sVertex)
        foundTarget = False
        while opened and (not foundTarget):  # continue till there are some opened vertexes
            v = opened.get()
            if self.pacing:
                print("processing: " + str(v.cords.x) + " " + str(v.cords.y))
            v.state = State.Processed
            freshNbrs = maze.getFreshNbs(v)
            for nb in freshNbrs:
                nb.state = State.Open
                nb.parent = v
                if nb == maze.tVertex:
                    foundTarget = True
                    if self.pacing:
                        print("!! Found target !!")
                    break  # found the target vertex
                opened.put(nb)
                if self.pacing:
                    print("opened nbr: " + str(nb.cords.x) + " " + str(nb.cords.y))
            if self.pacing:
                print("closed: " + str(v.cords.x) + " " + str(v.cords.y))
                maze.printMap()
                input()
                print()
            v.state = State.Closed  # closed opened vertex
        return maze

    def __algRandom(self, maze):
        opened = set()
        opened.add(maze.sVertex)
        foundTarget = False
        while opened and (not foundTarget): # continue till there are some opened vertexes
            openedList = list(opened)
            randI = random.randint(0, (len(openedList)-1))
            v = openedList[randI]
            if self.pacing:
                print("processing: " + str(v.cords.x) + " " + str(v.cords.y))
            opened.remove(v)
            v.state = State.Processed
            freshNbrs = maze.getFreshNbs(v)
            for nb in freshNbrs:
                nb.state = State.Open
                nb.parent = v
                if nb == maze.tVertex:
                    foundTarget = True
                    if self.pacing:
                        print("!! Found target !!")
                    break # found the target vertex
                opened.add(nb)
                if self.pacing:
                    print("opened nbr: " + str(nb.cords.x) + " " + str(nb.cords.y))
            if self.pacing:
                print("closed: " + str(v.cords.x) + " " + str(v.cords.y))
                maze.printMap()
                input()
                print()
            v.state = State.Closed  # closed opened vertex
        return maze
