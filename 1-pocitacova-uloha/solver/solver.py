from vertex import Vertex
from maze import Maze
from cords import Cords
from vertex import Vertex
from vertex import State
from pathFinder import PathFinder
from globs import *
import random

class Solver:

    # parses cords of start and target vertexes from input file with mate
    @staticmethod
    def __parseSTcords(stLines):
        cordsList = []
        for l in stLines:
            parts = l.split()
            print(str((parts[-2])[0:-1]))
            x = int((parts[-2])[0:-1])
            y = int(parts[-1])
            cordsList.append(Cords(x, y))
        return cordsList

    # parses 2D array representing maze from txt file on provided path
    # error -> False
    @staticmethod
    def prepareMaze(path):
        print("parsing maze from: " + path)
        print("original file:")
        with open(path) as file_in:
            mazeLines = []
            stLines = []
            for line in file_in:
                if(line[0] != wallChar): # no maze line
                    stLines.append(line)
                else: # maze line
                    mazeLines.append(line[0:-1]) # remove \n char
                print(line, end="")
        print()
        [sCords, tCords] = Solver.__parseSTcords(stLines)
        print("parsed sCords, tCords:")
        sCords.print()
        tCords.print()
        return Maze(mazeLines, sCords, tCords)


    # solves maze - fills path and all info into maze
    # returns maze with chagned states of vertexes and changed parents and all other info for solve
    @staticmethod
    def solveMaze(algId, maze, pacing):
        pFinder = PathFinder(pacing)
        maze = pFinder.findPathInMaze(algId, maze)
        return maze
