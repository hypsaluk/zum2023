from enum import Enum
from cords import Cords
from globs import *


# possible states of vertex in path-find algorithm (fresh - opened (found) - closed(solved))
class State(Enum):
    Fresh = 1
    Open = 2
    Closed = 3
    Processed = 4


# this class represents vertex in graph for path finding in 2D maze
# with needed attributes like info about parent, state etc.

class Vertex:
    cords = None  # Cords; the [x, y] coords in 2D maze
    isAccess = None  # bool; if the vertex is accessible (not blocked) for the path/agent
    parent = None  # Vertex; vertex which represent parent in found path
    inPath = False # bool; if this vertex is part of the final path
    state = State.Fresh # state of vertex during path finding (fresh - opened - closed)
    h = 0  # int; heuristic value
    g = 0  # int;

    def __init__(self, cords, isAccess):
        self.cords = cords
        self.isAccess = isAccess

    def print(self):
        print("vertex ", end="")
        # cords
        self.cords.print()
        # isAccess
        print("; isAccess: " + str(self.isAccess), end="")
        # parent
        print("; parent: ", end="")
        if self.parent == None:
            print("None", end="")
        else:
            self.parent.cords.print()
        # inPath
        print("; inPath: " + str(self.inPath), end="")
        # state
        print("; state: " + str(self.state), end="")

    def f(self):
        return self.h + self.g
