import random
import math
from checkers.GameBoard import GameBoard
from checkers.GameBoardGenerator import GameBoardGenerator
from checkers.PlayerColor import *
from checkers.Move import Move
from checkers.MoveCorrectnessEvaluator import MoveCorrectnessEvaluator
from checkers.GameStateEvaluator import GameStateEvaluator
from checkers.GameState import GameState

from checkers.Player import Player

"""
    AI player of checkers
    provides getMove() fc to generate next move for given board
    uses min-max algorithm with parametrized evaluate function, which will be further object of breeding
"""


class AIPlayer(Player):

    def __init__(self, playerColor: PlayerColor, params: list[float], depth: int):
        super().__init__(playerColor)
        self.params = params
        self.depth = depth

    def getMove(self, game_board: GameBoard):
        res = self.min_max(game_board, self.playerColor, self.depth)
        return res[0]

    # evaluates by float how good the position on board would be, depending on who is on turn
    # positive direction prefers white, negative prefers black
    def evaluate_board_score(self, game_board: GameBoard, color_on_turn: PlayerColor) -> float:
        params = self.params  # coeficients/weights
        all_moves_white = MoveCorrectnessEvaluator.get_all_possible_moves(game_board, PlayerColor.WHITE)
        all_moves_black = MoveCorrectnessEvaluator.get_all_possible_moves(game_board, PlayerColor.BLACK)

        """
            extract coefficients from input param array
        """
        w_stone_sum_coef = params[0]
        b_stone_sum_coef = params[1]
        w_reachable_sum_coef = params[2]
        b_reachable_sum_coef = params[3]

        """
            precount information like amount of stone, 
        """

        # positions on which stand players stones
        w_positions_standing = []
        b_positions_standing = []
        for row in range(game_board.boardSize):
            for col in range(game_board.boardSize):
                pos = game_board.board[row][col]
                if PlayerColor.isWhiteStone(pos):
                    w_positions_standing.append(pos)
                elif PlayerColor.isBLackStone(pos):
                    b_positions_standing.append(pos)

        # positions which are reachable by players stone
        w_positions_reachable = w_positions_standing  # we consider actually captured positions as reachable
        b_positions_reachable = b_positions_standing

        for w_mov in all_moves_white:
            new_pos = w_mov.to_pos
            is_original = True
            for marked_pos in w_positions_reachable:
                if marked_pos == new_pos:  # we have already marked this position
                    is_original = False
                    break
            if is_original:
                w_positions_reachable.append(new_pos)  # add new original to position to reachables
        # the same for black
        for b_mov in all_moves_black:
            new_pos = b_mov.to_pos
            is_original = True
            for marked_pos in b_positions_reachable:
                if marked_pos == new_pos:  # we have already marked this position
                    is_original = False
                    break
            if is_original:
                b_positions_reachable.append(new_pos)  # add new original to position to reachables

        w_num_of_stone = len(w_positions_standing)
        b_num_of_stone = len(b_positions_standing)
        w_reachable_sum = len(w_positions_reachable)
        b_reachable_sum = len(b_positions_reachable)
        """
            evaluate situation - win/loose (+inf/-inf)
        """
        # white would loose bcs of no stones
        if w_num_of_stone == 0:
            return -math.inf

        # black would loose bcs of no stones
        if b_num_of_stone == 0:
            return math.inf

        # no possible move -> player on turn would loos
        if not MoveCorrectnessEvaluator.has_possible_move(game_board, color_on_turn):
            return -math.inf if color_on_turn == PlayerColor.WHITE else math.inf

        # nobody would loose -> count scores using parameters
        """
            evaluate situation - how good is position
        """
        # 1) number of stones of each player
        w_num_of_stones_score = (w_num_of_stone * w_stone_sum_coef)
        b_num_of_stones_score = ((b_num_of_stone * b_stone_sum_coef) * -1)
        num_of_stones_score = w_num_of_stones_score + b_num_of_stones_score

        # 2) number of reachable position of both players
        w_num_of_reachable_score = (w_reachable_sum * w_reachable_sum_coef)
        b_num_of_reachable_score = ((b_reachable_sum * b_reachable_sum_coef) * -1)
        num_of_reachable_score = w_num_of_reachable_score + b_num_of_reachable_score

        # 3)

        """
            sum of results for every aspect 
        """
        # count sum of all score and return
        sum_score = num_of_stones_score + num_of_reachable_score
        return sum_score

    # by normal min-max algorithm finds the optimal next move, for player with color_on_turn
    # for checkers, the min-max has to be modified in sense that next move can be possibly played by the same player again (after stone capturing)
    # returns the optimal next move and value of board after applying the move
    def min_max(self, game_board: GameBoard, color_on_turn: PlayerColor, depth: int) -> (Move, int):
        # print(f"running min max on depth: {depth} where {color_on_turn}")
        #print("a")
        all_moves = MoveCorrectnessEvaluator.get_all_possible_moves(game_board, color_on_turn)
        #print("b")

        # no moves possible -> player on turn is defeated
        if len(all_moves) == 0:
            #print("c")
            game_state = GameStateEvaluator.evaluate_game_state(game_board, color_on_turn)
            if color_on_turn == PlayerColor.WHITE:
                if game_state == GameState.WHITE_WON:
                    score = math.inf
                else:
                    score = -math.inf
            if color_on_turn == PlayerColor.BLACK:
                if game_state == GameState.BLACK_WON:
                    score = math.inf
                else:
                    score = -math.inf
            return None, score

        #print("d")
        # take first move and evaluate it
        final_m = all_moves[0]
        final_m_board = GameBoardGenerator.applyMove(game_board, final_m)
        res = MoveCorrectnessEvaluator.evaluate_is_correct(game_board, final_m)
        final_on_turn = res[1]
        #print("e")
        if depth <= 1:
            #print("CALLINGA EVALIUATE FC")
            final_move_score = self.evaluate_board_score(final_m_board, final_on_turn)
        else:
            #print(f"calling min max from {final_m.from_pos} {final_m.to_pos} on depth {depth}")
            final_move_score = (self.min_max(final_m_board, final_on_turn, depth - 1))[1]

        # evaluate every possible move and pick the best one
        for i in range(0, len(all_moves)):
            #print("g")
            m = all_moves[i]
            m_board = GameBoardGenerator.applyMove(game_board, m)  # generate board after applying move
            res = MoveCorrectnessEvaluator.evaluate_is_correct(game_board, m)
            m_on_turn = res[1]  # get who would be on next move
            #print("h")
            if depth <= 1:
                m_score = self.evaluate_board_score(m_board, m_on_turn)
            else:
                # print(f"calling min max from {m.from_pos} {m.to_pos} on depth {depth}")
                m_score = (self.min_max(m_board, m_on_turn, depth - 1))[1]

            #print("i")
            # if better move, save it
            is_better = False
            # print(f"MSCORE for given board: {m_score}")
            # m_board.draw_board()

            #print("j")
            # white on turn
            if color_on_turn == PlayerColor.WHITE:
                if final_on_turn == PlayerColor.BLACK and m_on_turn == PlayerColor.WHITE \
                        and final_move_score > -math.inf:  # after will play white again
                    is_better = True
                else:  # same colors
                    if m_score >= final_move_score:  # if it has better score
                        # if the difference in score is 10 % or less, randomly choose move
                        if m_score == final_move_score:
                            is_better = True if random.random() < 0.5 else False
                        else:
                            is_better = True
                if is_better:
                    final_m = m
                    final_move_score = m_score
                    final_on_turn = m_on_turn
            is_better = False

            #print("k")
            # black on turn
            if color_on_turn == PlayerColor.BLACK:
                if final_on_turn == PlayerColor.WHITE and m_on_turn == PlayerColor.BLACK \
                        and final_move_score < math.inf:  # after will play black again
                    is_better = True
                else:  # same colors
                    if m_score <= final_move_score:  # if it has better score
                        # if the difference in score is 10 % or less, randomly choose move
                        if m_score == final_move_score:
                            is_better = True if random.random() < 0.5 else False
                        else:
                            is_better = True
                if is_better:
                    final_m = m
                    final_move_score = m_score
                    final_on_turn = m_on_turn

        #print("l")
        # print(f"best move for {color_on_turn}: {final_m.from_pos} {final_m.to_pos} on depth {depth} with value {final_move_score}")
        return final_m, final_move_score
