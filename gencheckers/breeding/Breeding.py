import random
from glob_methods import *
from typing import List
from breeding.Individual import Individual
from breeding.AIPlayer import AIPlayer
from checkers.PlayerColor import PlayerColor
from checkers.Arena import Arena
from checkers.GameBoardGenerator import GameBoardGenerator
from checkers.GameBoard import GameBoard
from checkers.GameState import GameState


class Breeding:
    def __init__(self, num_individuals: int, num_cycles: int, min_max_depth : int, params_length: int, param_value_limit: int):
        self.num_individuals = num_individuals
        self.num_cycles = num_cycles
        self.min_max_depth = min_max_depth
        self.params_length = params_length
        self.param_limit = param_value_limit
        self.num_of_fights = num_individuals

    # generates first generation of individuals for GA process
    def init_first_generation(self):
        individuals = []
        for i in range(self.num_individuals):
            params = [(random.randrange(-self.param_limit, self.param_limit)) for j in range(self.params_length)]
            individual = Individual(params)
            individuals.append(individual)
        return individuals

    def play_round(self, individuals):
        min_max_depth = self.min_max_depth
        arena = Arena()
        i = 0
        for ind1 in individuals:  # take every individual as first player and find him enemy
            for j in range(self.num_of_fights):  # run fights with him
                ind2 = ind1
                while ind1 == ind2:  # pick second individual
                    ind2 = individuals[random.randrange(0, len(individuals))]
                pl1 = ind1 if (random.random() < 0.5) else ind2  # random colors
                pl2 = ind2 if ind1 == pl1 else ind1
                ai1 = AIPlayer(PlayerColor.WHITE, pl1.params, min_max_depth)
                ai2 = AIPlayer(PlayerColor.BLACK, pl2.params, min_max_depth)
                print(f"playing game {i}")
                i += 1
                winner = arena.play(ai1, ai2)  # run game between those two players and find winner
                if winner == GameState.WHITE_WON:  # change fitness
                    pl1.add_fitness(1)
                    pl2.add_fitness(-1)
                else:
                    pl1.add_fitness(-1)
                    pl2.add_fitness(1)
        for ind in individuals:
            print(f"fittness: {ind.fitness}")

        return individuals

    def selection(self, individuals):
        individuals.sort(key=lambda x: x.fitness, reverse=True)
        for ind in individuals:
            print(f"fittness: {ind.fitness}")
        individuals = individuals[:self.num_individuals // 2]
        return individuals

    def crossover(self, individuals):
        new_generation = []
        for i in range(self.num_individuals // 2):
            parent1 = individuals[random.randrange(0, len(individuals))]
            parent2 = individuals[random.randrange(0, len(individuals))]
            child1_params = []
            child2_params = []

            for j in range(len(parent1.params)):
                if (random.random() < 0.5):
                    child1_params.append(parent1.params[j])
                    child2_params.append(parent2.params[j])
                else:
                    child1_params.append(parent2.params[j])
                    child2_params.append(parent1.params[j])

            # Create child individuals and add them to the new generation
            child1 = Individual(child1_params)
            child2 = Individual(child2_params)
            new_generation.append(child1)
            new_generation.append(child2)

        return new_generation


    def mutation(self, individuals):
        mutation_rate = 0.1
        for individual in individuals:
            for i in range(self.params_length):
                if random.random() < mutation_rate:
                    # Mutate the parameter by adding a random value within the limit
                    individual.params[i] += random.uniform(-self.param_limit, self.param_limit)

                    # Ensure the mutated parameter stays within the limit
                    individual.params[i] = max(0, min(self.param_limit, individual.params[i]))

        return individuals


    # main function for breeding
    def run_genetic_algorithm(self, individuals):
        for i in range(self.num_cycles):
            print(f"round {i}")
            print(f"========")
            print("phase: counting fitness")
            individuals = self.play_round(individuals)
            print("phase: selection")
            individuals = self.selection(individuals)
            print("phase: crossover")
            individuals = self.crossover(individuals)
            print("phase: mutation")
            individuals = self.mutation(individuals)
        return individuals


    def breed(self):
        init_gen = self.init_first_generation()  # init first individuals
        print("first generation inited")
        result_gen = self.run_genetic_algorithm(init_gen)  # breed better
        return init_gen, result_gen # return found optimal configurations
