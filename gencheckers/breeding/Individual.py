class Individual:

    def __init__(self, params: list[float]):
        self.params = params
        self.fitness = 0

    def add_fitness(self, val : int):
        self.fitness = (self.fitness + val)
