from checkers import Player
from checkers.GameStateEvaluator import GameStateEvaluator
from checkers.GameState import GameState
from checkers.GameBoard import GameBoard
from checkers.GameBoardGenerator import GameBoardGenerator
from checkers.MoveCorrectnessEvaluator import MoveCorrectnessEvaluator
from checkers.PlayerColor import PlayerColor
from breeding.AIPlayer import AIPlayer

"""
    this is a main class of checkers package
    it provides play() fc to run checkers game with two players (human or AI) and provides result 
    (white won / black won / draw)
"""

class Arena:
    def __init__(self):
        pass

    def play(self, p1: Player, p2: Player) -> GameState:
        game_board = GameBoard()
        color_on_turn = PlayerColor.BLACK
        game_state = GameState.IN_GAME

        while game_state == GameState.IN_GAME:
            # display AI recommended move
            """
            res = ai.min_max(game_board, color_on_turn, 3)
            move = res[0]
            print(f"AI move: {move.from_pos} {move.to_pos} with value {res[1]}")
            """
            # set next player to move
            player = p1 if color_on_turn == PlayerColor.WHITE else p2  # choose next player to be on turn
            #print(f"Player on turn: {color_on_turn}")
            #print(f"Game state: {game_state}")
            while True: # loop until you get correct move from player
                move = player.getMove(game_board) # get move
                res = MoveCorrectnessEvaluator.evaluate_is_correct(game_board, move) # try it
                is_correct_move = res[0]
                color_on_turn = res[1]
                if is_correct_move:  # if correct move, continue to apply move
                    break
            # apply move and evaluate game state
            game_board = GameBoardGenerator.applyMove(game_board, move)  # apply move
            game_state = GameStateEvaluator.evaluate_game_state(game_board, color_on_turn) # consider game state
            #print("ACTUAL GAME STATE: ")
            #game_board.draw_board()

        return game_state
