from numpy import char


class GameBoard:

    def __init__(self):
        self.boardSize = 8
        #self.board = [[' ' for j in range(self.boardSize)] for i in range(self.boardSize)]
        checkers_board = [
            ['w', '', 'w', '', 'w', '', 'w', ''],
            ['', 'w', '', 'w', '', 'w', '', 'w'],
            ['w', '', 'w', '', 'w', '', 'w', ''],
            ['', '', '', '', '', '', '', ''],
            ['', '', '', '', '', '', '', ''],
            ['', 'b', '', 'b', '', 'b', '', 'b'],
            ['b', '', 'b', '', 'b', '', 'b', ''],
            ['', 'b', '', 'b', '', 'b', '', 'b'],
        ]

        smaller_board = [['w', '', 'w', '', 'w'],
                         ['', 'w', '', 'w', ''],
                         ['', '', '', '', ''],
                         ['', 'b', '', 'b', ''],
                         ['b', '', 'b', '', 'b'],
                         ]

        self.board = smaller_board
        self.boardSize = len(self.board)

    def draw_board(self):
        # Draw chars as column labels
        print("  ", end="")
        for i in range(self.boardSize):
            print(chr(65 + i), end=' ')
        print()

        # Draw top border of board
        #print('  +' + '--+' * self.boardSize)

        # Iterate over rows and columns of board
        for i in range(self.boardSize):
            # Draw row label
            print(f'{i + 1}', end=' ')
            for j in range(self.boardSize):
                # Draw cell contents
                if char.isalpha(self.board[i][j]):
                    print(self.board[i][j], end=' ')
                else:
                    print('*', end=' ')
            # Draw row separator
            print()#print('\n  +' + '--+' * self.boardSize)

    @staticmethod
    def is_pawn(stone : char):
        if (stone.isalpha()):
            return (stone == stone.lower())
        return False

    @staticmethod
    def is_queen(stone : char):
        if (stone.isalpha()):
            return (stone == stone.upper())
        return False
