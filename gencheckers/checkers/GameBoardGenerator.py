import copy

from checkers.GameBoard import GameBoard
from checkers.Move import Move


class GameBoardGenerator:
    @staticmethod
    def applyMove(game_board: GameBoard, move: Move) -> GameBoard:
        from_row, from_col = move.from_pos
        to_row, to_col = move.to_pos
        # check out of range
        if from_row < 0 or from_row >= game_board.boardSize or from_col < 0 or from_col >= game_board.boardSize or to_row < 0 or \
                to_row >= game_board.boardSize or to_col < 0 or to_col >= game_board.boardSize:
            raise ValueError("Move out of range")
        # move stone from from_pos to to_pos
        game_board = copy.deepcopy(game_board) # ! makes deepcopy, so no influence on parameter game_board
        game_board.board[to_row][to_col] = game_board.board[from_row][from_col]
        game_board.board[from_row][from_col] = ''
        # Calculate the distance and direction of the move
        row_delta = to_row - from_row
        col_delta = to_col - from_col
        distance = abs(row_delta)
        direction = (row_delta // distance, col_delta // distance)
        # for each stone on diagonal, remove it
        for i in range(0, distance-1):
            a = from_row + direction[0]
            b = from_col + direction[1]
            game_board.board[a][b] = ''
        # get new board
        return game_board
