from enum import Enum

class GameState(Enum):
    IN_GAME = 0
    WHITE_WON = 1
    BLACK_WON = 2
    DRAW = 3
