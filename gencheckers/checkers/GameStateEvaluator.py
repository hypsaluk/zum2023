from enum import Enum
from checkers.GameBoard import GameBoard
from checkers.PlayerColor import PlayerColor
from checkers.GameState import GameState
from checkers.MoveCorrectnessEvaluator import MoveCorrectnessEvaluator

class GameStateEvaluator:
    @staticmethod
    def evaluate_game_state(game_board: GameBoard, color_on_turn : PlayerColor) -> GameState:
        # count sum of white stones and black stones
        white_count = 0
        black_count = 0
        for i in range(game_board.boardSize):
            for j in range(game_board.boardSize):
                if game_board.board[i][j] == 'w' or game_board.board[i][j] == 'W':
                    white_count += 1
                elif game_board.board[i][j] == 'b' or game_board.board[i][j] == 'B':
                    black_count += 1

        # check if both players have at least one stone
        if white_count == 0:
            return GameState.BLACK_WON
        elif black_count == 0:
            return GameState.WHITE_WON

        # Check if there are no valid moves left for both players
        white_has_valid_moves = MoveCorrectnessEvaluator.has_possible_move(game_board, PlayerColor.WHITE, False)
        black_has_valid_moves = MoveCorrectnessEvaluator.has_possible_move(game_board, PlayerColor.BLACK, False)

        if not white_has_valid_moves:
            return GameState.BLACK_WON
        if not black_has_valid_moves:
            return GameState.WHITE_WON

        return GameState.IN_GAME
