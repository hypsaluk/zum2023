from checkers.Player import *
from checkers.Move import *

class HumanPlayer(Player):

    # prompts human player to input his move, returns final Move
    def getMove(self, board) -> Move:
        board.draw_board()
        move_str = input("Please write move in form 'XY XY', e.g.: A3 B4\n")
        try:
            from_row = ord(move_str[1]) - ord('1')
            from_col = ord(move_str[0]) - ord('A')
            to_row = ord(move_str[4]) - ord('1')
            to_col = ord(move_str[3]) - ord('A')
            move = Move(self.playerColor, (from_row, from_col), (to_row, to_col))
        except (ValueError, IndexError):
            print("Invalid input, it has to be in form: A3 B4.")
        return move

