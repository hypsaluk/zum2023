from numpy import char
from typing import List

from checkers.GameBoard import GameBoard
from checkers.Move import Move
from checkers.PlayerColor import PlayerColor


class MoveCorrectnessEvaluator:

    @staticmethod
    def isPlayersStone(char, playerColor):
        if ((char == "w" or char == "W") and (playerColor == PlayerColor.WHITE)):
            return True
        if ((char == "b" or char == "B") and (playerColor == PlayerColor.BLACK)):
            return True
        return False

    @staticmethod
    def is_valid_direction(move: Move) -> bool:
        player_color = move.playerColor
        direction = (move.to_pos[0] - move.from_pos[0], move.to_pos[1] - move.from_pos[1])
        if player_color == PlayerColor.WHITE:
            return direction[0] == 1 and abs(direction[1]) == 1
        else:
            return direction[0] == -1 and abs(direction[1]) == 1

    @staticmethod
    def is_valid_jump(game_board: GameBoard, move: Move) -> bool:
        player_color = move.playerColor
        direction = (move.to_pos[0] - move.from_pos[0], move.to_pos[1] - move.from_pos[1])
        middle_pos = ((move.from_pos[0] + move.to_pos[0]) // 2, (move.from_pos[1] + move.to_pos[1]) // 2)
        if player_color == PlayerColor.WHITE:
            return direction[0] == 2 and abs(direction[1]) == 2 and MoveCorrectnessEvaluator.isPlayersStone(
                game_board.board[middle_pos[0]][
                    middle_pos[1]],
                PlayerColor.BLACK)
        else:
            return direction[0] == -2 and abs(direction[1]) == 2 and MoveCorrectnessEvaluator.isPlayersStone(
                game_board.board[middle_pos[0]][
                    middle_pos[1]],
                PlayerColor.WHITE)

    @staticmethod
    def evaluate_is_correct(game_board, move, allowPrint=True) -> (bool, PlayerColor):
        # Check if move is out of bounds
        for pos in [move.from_pos, move.to_pos]:
            if pos[0] < 0 or pos[0] >= game_board.boardSize or pos[1] < 0 or pos[1] >= game_board.boardSize:
                if allowPrint:
                    print("ERROR: out of range")
                return (False, move.playerColor)

        # Check if from position contains player's piece
        fromBoxChar = game_board.board[move.from_pos[0]][move.from_pos[1]]
        if not MoveCorrectnessEvaluator.isPlayersStone(fromBoxChar, move.playerColor):
            if allowPrint:
                print("ERROR: not players stone on from")
            return (False, move.playerColor)
        toBoxChar = game_board.board[move.to_pos[0]][move.to_pos[1]]
        # to position is blocked by other stone
        if char.isalpha(toBoxChar):
            if allowPrint:
                print("ERROR: on to position is other stone")
            return (False, move.playerColor)

        # check if stone on from position can correctly examine move to the to position
        if (fromBoxChar == fromBoxChar.lower()):  # its normal piece, not queen
            distance = abs(move.to_pos[0] - move.from_pos[0])
            if distance == 1:  # check normal move -> other player moves
                if MoveCorrectnessEvaluator.is_valid_direction(move):
                    otherPlayerColor = PlayerColor.BLACK if (
                            move.playerColor == PlayerColor.WHITE) else PlayerColor.WHITE
                    return (True, otherPlayerColor)
                else:
                    if allowPrint:
                        print("ERROR: not valid direction")
                    return (False, move.playerColor)
            elif distance == 2:  # check jump over other piece -> next move for same player
                if MoveCorrectnessEvaluator.is_valid_jump(game_board, move):
                    return (True, move.playerColor)
                else:
                    if allowPrint:
                        print("ERROR: not valid jump")
                    return (False, move.playerColor)
            else:  # too far for a normal piece
                if allowPrint:
                    print("ERROR: the distance cant be " + str(distance) + "for normal piece")
                return (False, move.playerColor)
        else:  # for now we dont consider a queen
            if allowPrint:
                print("ERROR: we dont play with queens")
            return (False, move.playerColor)

    # returns list of all possible moves for player with given color on given board
    @staticmethod
    def get_all_possible_moves(game_board: GameBoard, player_color: PlayerColor, allowPrint=False) -> List[Move]:
        moves = []
        for row in range(game_board.boardSize):
            for col in range(game_board.boardSize):
                stone = game_board.board[row][col]
                if MoveCorrectnessEvaluator.isPlayersStone(stone, player_color): # player has stone on given position
                    if stone == stone.lower():  # its normal stone, not queen
                        for dx, dy in [(1, 1), (1, -1), (-1, -1), (-1, 1)]:  # in all 4 directions
                            for coef in [1, 2]:  # try to make a) normal step b) jump
                                new_row, new_col = row + dx * coef, col + dy * coef
                                move = Move(player_color, (row, col), (new_row, new_col))
                                if MoveCorrectnessEvaluator.evaluate_is_correct(game_board, move, allowPrint)[0]:
                                    #print(f"yes for {move.from_pos} {move.to_pos}")
                                    moves.append(move)
                    # elif stone == stone.upper(): TBD for queen
        return moves

    # True if player with given color on given board can play at least one move
    # TBD merge with 'get_all_possible_moves' fc to make it faster
    @staticmethod
    def has_possible_move(game_board: GameBoard, player_color: PlayerColor, allowPrint=False) -> bool:
        return len(MoveCorrectnessEvaluator.get_all_possible_moves(game_board, player_color, allowPrint)) > 0
