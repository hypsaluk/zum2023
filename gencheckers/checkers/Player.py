from abc import ABC, abstractmethod
import random

class Player(ABC):

    def __init__(self, playerColor):
        self.playerColor = playerColor

    @abstractmethod
    def getMove(self, board):
        pass
