from enum import Enum

from numpy import char


class PlayerColor(Enum):
    BLACK = 1
    WHITE = 2

    @staticmethod
    def isWhiteStone(stone: char) -> bool:
        return stone.lower() == 'w'

    @staticmethod
    def isBLackStone(stone: char) -> bool:
        return stone.lower() == 'b'
