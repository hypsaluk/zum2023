def module_num(num, mod):
    if num >= 0: # positive
        res = (num % mod)

    else: # negative
        pos_num = (-num)
        pos_res = (pos_num % mod)
        res = -pos_res

    return res
