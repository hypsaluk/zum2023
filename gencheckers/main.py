import copy

from checkers.Arena import *
from checkers.HumanPlayer import *
from breeding.AIPlayer import AIPlayer
from breeding.Breeding import Breeding
from breeding.Individual import Individual


def run_one_evolution():
    """
    running of breeding
    """

    num_of_individuals = 2
    num_of_cycles = 2
    min_max_depth = 3
    params_array_len = 4
    params_val_limit = 5

    br = Breeding(num_of_individuals, num_of_cycles, min_max_depth, params_array_len, params_val_limit)
    init_gen, result_gen = br.breed()

    for i, ind in enumerate(init_gen):
        print(f"{ind.params}")

    for i, ind in enumerate(result_gen):
        print(f"{ind.params}")

    rand_init_ind = init_gen[random.randrange(0, len(init_gen))]
    rand_result_ind = result_gen[random.randrange(0, len(result_gen))]

    # init player vs breeded player in x rounds
    ind1 = copy.deepcopy(rand_init_ind)
    ind1.fitness = 0
    ind2 = copy.deepcopy(rand_result_ind)
    ind2.fitness = 0
    arena = Arena()
    num_of_control_plays = 10
    for i in range(0, num_of_control_plays):
        pl1 = ind1 if (random.random() < 0.5) else ind2  # random colors
        pl2 = ind2 if ind1 == pl1 else ind1
        ai1 = AIPlayer(PlayerColor.WHITE, pl1.params, min_max_depth)
        ai2 = AIPlayer(PlayerColor.BLACK, pl2.params, min_max_depth)
        print(f"playing game {i}")
        i += 1
        winner = arena.play(ai1, ai2)  # run game between those two players and find winner
        if winner == GameState.WHITE_WON:  # change fitness
            pl1.add_fitness(1)
            pl2.add_fitness(-1)
        else:
            pl1.add_fitness(-1)
            pl2.add_fitness(1)

    print("results:\n=====")
    print(f"individual from init generation score: {ind1.fitness}")
    print(f"individual from bred generation score: {ind2.fitness}")
    return ind2.fitness

if __name__ == '__main__':

    num_of_evolutions = 1
    res_arr = []
    for i in range(num_of_evolutions):
        print(f"RUNNING OF {i}. EVOLUTION")
        res = run_one_evolution()
        res_arr.append(res)

    sum_of_evolutions_won_by_final_gen = 0
    sum_of_draws = 0
    sum_of_evolutions_won_by_initial_gen = 0
    for i, res in enumerate(res_arr):
        if (res > 0):
            sum_of_evolutions_won_by_final_gen += 1
            print(f"in {i}. won final")
        elif (res == 0):
            print(f"in {i}. were draw")
            sum_of_draws += 1
        elif (res < 0):
            print(f"in {i}. won initial")
            sum_of_evolutions_won_by_initial_gen += 1
    print(f"final won {sum_of_evolutions_won_by_final_gen} out of {len(res_arr)}")
    print(f"there were {sum_of_draws} draws out of {len(res_arr)}")
    print(f"initial won {sum_of_evolutions_won_by_initial_gen   } out of {len(res_arr)}")

    """
    playing of two AIs
    
    min_max_depth = 4
    params = [1, 1, 1, 1]
    ai1 = AIPlayer(PlayerColor.WHITE, params, min_max_depth)
    ai2 = AIPlayer(PlayerColor.BLACK, params, min_max_depth)
    p1 = ai1
    p2 = ai2
    arena = Arena()
    print(f"Running game for two AIs")
    final_state = arena.play(ai1, ai2)
    print(f"The game ended like: {final_state}")
    """

    """
    game_board = GameBoard()
    game_board.draw_board()
    moves = MoveCorrectnessEvaluator.get_all_possible_moves(game_board, PlayerColor.WHITE, False)
    for m in moves:
        print(m.from_pos, m.to_pos)

    res = a1.min_max(game_board, PlayerColor.BLACK, 3)
    move = res[0]
    print(f"AI move: {move.from_pos} {move.to_pos} {res[1]}")
    """

